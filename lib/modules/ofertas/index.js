module.exports = {
  extend: 'apostrophe-pieces',
  label: 'Oferta',
  name: 'ofertas',
  alias: 'ofertas',
  // contextualOnly: true,
  addFields: [
  {
      name: 'imagen',
      label: 'Insertar la imagen',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      required:true
      // textarea: true      
    },
    {
      name: 'nombreOfertas',
      label: 'Insertar el Nombre del Destino',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      required:true
      // textarea: true
      
    },
    {
      name: 'Lugar',
      label: 'Insertar lugar',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      textarea: true
      
    },
    {
      name: 'cantidadPersonas',
      label: 'Insertar la cantidad de Personas',
      // help: 'Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
      type: 'string',
      // textarea: true
      
    },
    {
      name: 'calendar',
      label: 'Insertar el tiempo de reservación',
      // help: 'Inserte el Tipo de Elemento html a insertar en el documento',
      type: 'string'
      // textarea: true
      
    },
    {
      name: 'regalo',
      label: 'Insertar Regalo',
      // help: 'Inserte el Tipo de Elemento html a insertar en el documento',
      type: 'string'
      // textarea: true
      
    }
    
  ],arrangeFields: [
    {
      name: 'dest',
      label: 'Oferta',
      fields: ['imagen', 'nombreOfertas', 'Lugar', 'cantidadPersonas', 'calendar',  ]
    }
  ],construct: function(self, options) {
    self.beforeSave = function(req, piece, options, callback) {
      piece.title = piece.nombreOfertas ;
      piece.slug = piece.nombreOfertas ;
      return callback();
    };
  }
};

