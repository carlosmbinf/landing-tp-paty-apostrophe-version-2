module.exports = {
 extend: 'apostrophe-pieces-widgets',
piecesModuleName :'ofertas',
  label: 'TIPO DE CARD CON OFERTAS',
  // contextualOnly: true,
  addFields: [
  {
    name: 'style',
    label: 'Estilos . \n Porfavor, use selectores para q no haya coincidencias con otros componentes en la Páguina ',
    type: 'string'
    // required: true,
    // contextual: true
  },
  {
    name: 'id',
    label: 'ID del elemento',
    type: 'string'
    // required: true,
    // contextual: true
  },{
    name: 'link',
    label: 'LINK del elemento',
    type: 'string'
    // required: true,
    // contextual: true
  },{
    name: 'cardsnumbers',
    label: 'Inserte la cantidad de Cards',
    type: 'integer'
    // required: true,
    // contextual: true
  }
  ]};

