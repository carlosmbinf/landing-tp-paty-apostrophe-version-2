module.exports = {
    // extend: 'apostrophe-pieces',
    name: 'clients',
    label: 'clients',
    alias: 'clients',
    //Estos campos por ahora se estan ignorando
    //la collection acepta cualquier cosa

    construct: function (self, options) {




        self.apos.app.post('/clients/insert', function (req, res) {
            self.apos.db.collection('clients', (error, collection) => {
                if (error) {
                    res.send({ status: 400, error: error.message });
                }

                // if(req.session.captcha == req.body.text ){
                    collection.insert({ createdAt: new Date(), ...req.body });
                    res.send({ status: 200 })
                // }else{
// res.send({ status: 400, error: "Error en el captcha"});
                // }

                
            })
        });

        self.apos.app.post('/clients/delete', function (req, res) {


            var ObjectID = require('mongodb').ObjectID


            self.apos.db.collection('clients', (error, collection) => {

                if (error) {
                    res.send({ status: 400, error: error.message });
                }

                    for(var i = 0; i < req.body.length; i++) {

                        // console.log("-------------------Elemento Eliminado: ---------------------" + "\n" + req.body[i])
                        try {

                            var objectId = new ObjectID(req.body[i]._id);

                            collection.deleteOne({ "_id" : objectId }) 
                                // statements
                            } catch(e) {
                                // statements
                                console.log(e);
                            }
                                   // (o el campo que necesites)

                    }



                res.send({ status: 200 })


            })
        });

        self.apos.app.get('/mongo', function (req, res) {


            var MongoClient = require('mongodb').MongoClient;
            var url = "mongodb://mongo:27017";

            MongoClient.connect(url, function(err, db) {
              if (err) throw err;
              var dbo = db.db("landyng_tp_paty");

                dbo.collection("clients").find({}).toArray(function(err, result) {

                    if (err) throw err;                   
                res.status(200).send(result);
                db.close();

                });
            });




        });



    }
}

